import os
import sys
from pathlib import Path
from typing import Any, Dict, List

import tomli
from babel.messages.frontend import CommandLineInterface as Babel
from buildtools import log, os_utils
from buildtools.maestro import BuildMaestro, shell
from buildtools.maestro.base_target import SingleBuildTarget
from buildtools.maestro.fileio import CopyFileTarget, ReplaceTextTarget
from buildtools.maestro.nuitka import NuitkaTarget

AVAILABLE_LANGUAGES = ["en_US"]
LANGFILES: List[str] = []
LOCALE_DIR = Path("locale")

TMP_DIR = Path(".tmp")
MOD_PATH = Path("melontool") / "__main__.py"
TMP_EXE = Path(".tmp") / "__main__.dist" / "melontool"
DIST_DIR = Path("dist")
if os.name == "nt":
    TMP_EXE = TMP_EXE.with_suffix(".exe")


class BabelCompileTarget(SingleBuildTarget):
    BT_LABEL = "BABEL"

    def __init__(self, outputfile: Path, inputfile: Path, dependencies: List[str] = []):
        self.inputfile: Path = inputfile
        self.outputfile: Path = outputfile
        super().__init__(
            target=str(outputfile), files=[str(inputfile)], dependencies=dependencies
        )

    def build(self):
        Babel().run(
            ["pybabel", "-q", "compile", "-i", self.inputfile, "-o", self.outputfile]
        )


def buildDomain(bm: BuildMaestro, domain: str) -> None:
    global LANGFILES
    for lang in AVAILABLE_LANGUAGES:
        outf = LOCALE_DIR / lang / "LC_MESSAGES" / f"{domain}.mo"
        inf = LOCALE_DIR / lang / "LC_MESSAGES" / f"{domain}.po"
        LANGFILES.append(bm.add(BabelCompileTarget(outf, inf)).target)


bm = BuildMaestro()

bm.other_dirs_to_clean.append(".tmp")
bm.other_dirs_to_clean.append("dist")

# gettext stuff.
buildDomain(bm, 'melontool.__main__')
buildDomain(bm, "melontool.gameids")
buildDomain(bm, "melontool.projectconfig")
buildDomain(bm, "melontool.steamlibrary")

data: Dict[str, Any] = {}
with open("pyproject.toml", "rb") as f:
    data = tomli.load(f)

PATH_CONSTS_OUT = Path("melontool") / "consts.py"
PATH_CONSTS_IN = Path("melontool") / "consts.py.in"
consts = bm.add(
    ReplaceTextTarget(
        str(PATH_CONSTS_OUT),
        str(PATH_CONSTS_IN),
        replacements={
            r"{{VERSION_STR}}": repr(data["tool"]["poetry"]["version"]),
            r"{{VERSION_TUPLE}}": repr(
                tuple(map(int, data["tool"]["poetry"]["version"].split(".")))
            ),
        },
    )
)

files = [str(x.absolute()) for x in Path("melontool").glob("*.py")]
melontool_n = bm.add(
    NuitkaTarget(
        str(MOD_PATH), "__main__", files, dependencies=[consts.target], single_file=True
    )
)
melontool_n.windows_company_name = "MelonTool Contributors"
melontool_n.windows_product_name = "MelonTool Project Assistant"
melontool_n.windows_file_description = "Automates MelonLoader mod project maintenance"
melontool_n.windows_file_version = (1, 0, 0, 0)
MELONTOOL_EXE = (DIST_DIR / "melontool").with_suffix(
    ".exe" if os_utils.is_windows() else ""
)
bm.add(
    CopyFileTarget(
        str(MELONTOOL_EXE),
        str(melontool_n.executable_mangled),
        dependencies=[str(melontool_n.executable_mangled)],
    )
).target
bm.as_app()

# mv __main__.exe melontool.exe
# rm -rfv __main__.*
