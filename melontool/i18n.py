from gettext import GNUTranslations, translation
from pathlib import Path
from typing import Callable, List, Tuple
from melontool.consts import AVAILABLE_LANGUAGES

TXN_DIR: Path = Path(__file__).parent.parent / 'locale'


def getTxnFor(modname) -> Tuple[GNUTranslations, Callable[[str], str]]:
    '''
    Shortcut to obtain t and _ in modules.
    '''
    t = translation(modname, localedir=TXN_DIR, languages=AVAILABLE_LANGUAGES)
    _ = t.gettext
    return (t, _)
