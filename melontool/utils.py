from pathlib import Path
from typing import Callable


# from melontool.i18n import getTxnFor #isort: skip
# t,_ = getTxnFor(__name__)
def _(a: str) -> str:
    return _

def existingFilePath(argid: str) -> Callable[[str], Path]:
    def a(inp: str) -> Path:
        p = Path(inp)
        assert p.is_file(), _(f'{argid} - File {p} does not exist.')
        return p
    return a

def existingDirPath(argid: str) -> Callable[[str], Path]:
    def a(input: str) -> Path:
        p = Path(input)
        assert p.is_dir(), _(f'{argid} - Directory {p} does not exist.')
        return p
    return a