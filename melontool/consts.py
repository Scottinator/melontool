﻿from typing import FrozenSet, Tuple, List, Final
from pathlib import Path


NAME: str = 'MelonTool'
AUTHORS: FrozenSet[str] = frozenset({"Scottinator <scgriffin213@outlook.com>",})
VERSION: Tuple[int, int, int] = (0, 0, 1)
VERSION_STR: str = '0.0.1'

# Keep in sync with BUILD_LOCALE.py
AVAILABLE_LANGUAGES: List[str] = ['en_US',]


BREAKS_SHIT = frozenset(["Newtonsoft.Json"])
DEFAULT_DNF: Final[str] = "net48"
DEFAULT_DNF48_REFS: FrozenSet[str] = frozenset(
    {
        "Microsoft.CSharp",
        "mscorlib",
        "System.Core",
        "System.Data.DataSetExtensions",
        "System.Data",
        "System.Net.Http",
        "System.Xml.Linq",
        "System.Xml",
        "System",
    }
)
DEFAULT_GAME_REFS: FrozenSet[str] = frozenset(
    {
        "Assembly-CSharp",
        "UnityEngine",
    }
)
PROJCFG_FILE = Path("melontool.yml")
LOCALPROJCFG_FILE = Path("melontool.local")