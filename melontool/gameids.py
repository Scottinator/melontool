from enum import Enum
import os
from pathlib import Path
from typing import Dict, Optional

# from melontool.i18n import getTxnFor  # isort: skip
# t, _ = getTxnFor(__name__)

WIN_FILE_RESERVED = '<>:"/\\|?*'
WIN_DIR_RESERVED = WIN_FILE_RESERVED + "."
WIN_FILE_TRANS: Dict[str, str] = {
    chr(c): "_" for c in range(256) if chr(c) in WIN_FILE_RESERVED or c <= 31
}


def fix_file_name(n: str) -> str:
    if os.name == "nt":
        return n.translate(WIN_FILE_TRANS)
    else:
        return n


WIN_DIR_TRANS: Dict[str, str] = {
    chr(c): "_" for c in range(256) if chr(c) in WIN_DIR_RESERVED or c <= 31
}


def fix_dir_name(n: str) -> str:
    if os.name == "nt":
        return n.translate(WIN_DIR_TRANS)
    else:
        return n


class UnityGameInfo:
    def __init__(
        self,
        appid: int,
        company: str,
        name: str,
        exe_stem: str,
        is_il2cpp: bool=True,
        steamdir_name: Optional[str] = None
    ) -> None:
        self.appid = appid
        self.company = company
        self.name = name
        # Used to calculate data directory location, since Unity just slaps _Data on the end of the filename.
        self.exe_stem = exe_stem
        self.is_il2cpp = is_il2cpp
        self.steam_dir_name: str = steamdir_name or self.name

    @property
    def local_low_dir(self) -> Path:
        if os.name == "nt":
            return (
                Path(os.getenv("LOCALAPPDATA") + "Low")
                / fix_dir_name(self.company)
                / fix_dir_name(self.name)
            )
        else:
            return (
                Path.home()
                / ".config"
                / "unity3d"
                / fix_dir_name(self.company)
                / fix_dir_name(self.name)
            )
        # raise NotImplemented(_('Please feel free to implement this via a merge request. I do not have much experience with Unity on Linux/Mac!'))

    def getDataDir(self, install_dir: Path) -> Path:
        return install_dir / f"{self.exe_stem}_Data"


# fmt: off
class EGameID(Enum):
    UNKNOWN = None
    #                             AppID    Company                         Product                                  EXE Stem
    AUDICA        = UnityGameInfo(1020340, "Harmonix Music Systems, Inc.", "Audica",                                "Audica",       is_il2cpp=True)
    BONELAB       = UnityGameInfo(1592190, "Stress Level Zero",            "BONELAB",                               "BONELAB",      is_il2cpp=True)
    BTD6          = UnityGameInfo(960090,  "Ninja Kiwi",                   "BloonsTD6",                             "BloonsTD6",    is_il2cpp=True)
    CHILLOUTVR    = UnityGameInfo(661130,  "Alpha Blend Interactive",      "ChilloutVR",                            "ChilloutVR",   is_il2cpp=False)
    DEMEO         = UnityGameInfo(1484280, "Resolution Games",             "Demeo",                                 "demeo",        is_il2cpp=False)
    H3VR          = UnityGameInfo(450540,  "RUST LTD",                     "Hot Dogs Horseshoes and Hand Grenades", "h3vr",         is_il2cpp=False)
    JOB_SIMULATOR = UnityGameInfo(448280,  "Owlchemy Labs",                "Job Simulator",                         "JobSimulator", is_il2cpp=True)
    PISTOL_WHIP   = UnityGameInfo(1079800, "Cloudhead Games, Ltd.",        "Pistol Whip",                           "Pistol Whip",  is_il2cpp=True)
    TLD           = UnityGameInfo(305620,  "Hinterland",                   "TheLongDark",                           "tld",          is_il2cpp=True)
    #VRCHAT       = UnityGameInfo(438100,  "VRChat",                       "VRChat",                                "VRChat",       is_il2cpp=True) - Verboten
# fmt: on
