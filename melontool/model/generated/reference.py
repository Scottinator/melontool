from melontool.model._base import BaseXMLModel
from pathlib import Path
from typing import Optional
__ALL__ = ['RawReference']
class RawReference(BaseXMLModel):
    def _get_include(self) -> Optional[Path]:
        return self._getAttrPath('Include')
    def _set_include(self,  value: Optional[Path]) -> None:
        self._setAttrPath('Include', value)
    include = property(_get_include, _set_include)
    def _get_aliases(self) -> Optional[str]:
        return self._getEString('Aliases')
    def _set_aliases(self,  value: Optional[str]) -> None:
        self._setEString('Aliases', value)
    aliases = property(_get_aliases, _set_aliases)
    def _get_fusionName(self) -> Optional[str]:
        return self._getEString('FusionName')
    def _set_fusionName(self,  value: Optional[str]) -> None:
        self._setEString('FusionName', value)
    fusionName = property(_get_fusionName, _set_fusionName)
    def _get_hintPath(self) -> Optional[Path]:
        return self._getEPath('HintPath')
    def _set_hintPath(self,  value: Optional[Path]) -> None:
        self._setEPath('HintPath', value)
    hintPath = property(_get_hintPath, _set_hintPath)
    def _get_name(self) -> Optional[str]:
        return self._getEString('Name')
    def _set_name(self,  value: Optional[str]) -> None:
        self._setEString('Name', value)
    name = property(_get_name, _set_name)
    def _get_private(self) -> Optional[bool]:
        return self._getEBool('Private')
    def _set_private(self,  value: Optional[bool]) -> None:
        self._setEBool('Private', value)
    private = property(_get_private, _set_private)
    def _get_specificVersion(self) -> Optional[bool]:
        return self._getEBool('SpecificVersion')
    def _set_specificVersion(self,  value: Optional[bool]) -> None:
        self._setEBool('SpecificVersion', value)
    specificVersion = property(_get_specificVersion, _set_specificVersion)
