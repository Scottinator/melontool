from melontool.model._base import BaseXMLModel
from pathlib import Path
from typing import Optional
__ALL__ = ['RawProject']
class RawProject(BaseXMLModel):
    def _get_sdk(self) -> Optional[str]:
        return self._getAttrString('Sdk')
    def _set_sdk(self,  value: Optional[str]) -> None:
        self._setAttrString('Sdk', value)
    sdk = property(_get_sdk, _set_sdk)
