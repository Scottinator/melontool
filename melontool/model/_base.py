from typing import Optional
from lxml import etree
from pathlib import Path
class BaseXMLModel(etree.ElementBase):
    ####
    # ATTRIBUTES
    def _getAttrString(self, attrname: str, default: Optional[str] = None) -> None:
        return self.get(attrname, default)
    def _setAttrString(self, attrname: str, value: Optional[str] = None) -> None:
        if value is None:
            if attrname in self._element.attrib:
                del self.attrib[attrname]
        else:
            self.set(attrname, value)

    def _getAttrBool(self, attrname: str, default: Optional[bool] = None) -> Optional[bool]:
        v = self._getAttrString(attrname, default)
        return None if v is None else bool(v)
    def _setAttrBool(self, attrname: str, value: Optional[bool] = None) -> None:
        self._setAttrString(attrname, None if value is None else (str(value).lower()))

    def _getAttrPath(self, attrname: str, default: Optional[Path] = None) -> Optional[Path]:
        v = self._getAttrString(attrname, default)
        return None if v is None else Path(v)
    def _setAttrPath(self, attrname: str, value: Optional[Path] = None) -> None:
        self._setAttrString(attrname, None if value is None else (str(value)))

    def _getAttrInteger(self, attrname: str, default: Optional[int] = None) -> Optional[int]:
        v = self._getAttrString(attrname, default)
        return None if v is None else int(v)
    def _setAttrInteger(self, attrname: str, value: Optional[int] = None) -> None:
        self._setAttrString(attrname, None if value is None else (str(value)))

    def _getAttrFloat(self, attrname: str, default: Optional[float] = None) -> Optional[float]:
        v = self._getAttrString(attrname, default)
        return None if v is None else float(v)
    def _setAttrFloat(self, attrname: str, value: Optional[float] = None) -> None:
        self._setAttrString(attrname, None if value is None else (str(value)))

    ####
    # SUBELEMENTS
    def _getEString(self, tagname: str, default: Optional[str] = None) -> Optional[str]:
        return self.findtext(tagname, default)
    def _setEString(self, tagname: str, value: Optional[str] = None) -> None:
        element: etree.ElementBase
        if value is None:
            if (element := self.find(tagname)) is not None:
                element.getparent().remove(element)
        else:
            if (element := self.find(tagname)) is None:
                element = etree.SubElement(self, tagname)
            element.text = value

    def _getEBool(self, tagname: str, default: Optional[bool] = None) -> Optional[bool]:
        v = self._getEString(tagname, default)
        return None if v is None else bool(v)
    def _setEBool(self, tagname: str, value: Optional[bool] = None) -> None:
        self._setEString(tagname, None if value is None else (str(value).lower()))

    def _getEPath(self, tagname: str, default: Optional[Path] = None) -> Optional[Path]:
        v = self._getEString(tagname, default)
        return None if v is None else Path(v)
    def _setEPath(self, tagname: str, value: Optional[Path] = None) -> None:
        self._setEString(tagname, None if value is None else (str(value)))

    def _getEInteger(self, tagname: str, default: Optional[int] = None) -> Optional[int]:
        v = self._getEString(tagname, default)
        return None if v is None else int(v)
    def _setEInteger(self, tagname: str, value: Optional[int] = None) -> None:
        self._setEString(tagname, None if value is None else (str(value)))

    def _getEFloat(self, tagname: str, default: Optional[float] = None) -> Optional[float]:
        v = self._getEString(tagname, default)
        return None if v is None else float(v)
    def _setEFloat(self, tagname: str, value: Optional[float] = None) -> None:
        self._setEString(tagname, None if value is None else (str(value)))

    def addTo(self, parent: etree.ElementBase) -> None:
        parent.append(self)
