import tabulate
from pathlib import Path
from typing import Any, Dict, Optional, Set, Union
import click
import steamclient
import vdf

from melontool.gameids import EGameID, UnityGameInfo

from melontool.i18n import getTxnFor

t, _ = getTxnFor(__name__)

__all__ = ["SteamLibrary"]


class SteamApp:
    def __init__(self) -> None:
        self.id: int = 0
        self.name: str = ""
        self.path: Optional[Path] = None
        self.unityInfo: Optional[UnityGameInfo] = None

    def setUnityInfo(self, ugi: Union[int, EGameID, UnityGameInfo]) -> None:
        if isinstance(ugi, int):
            self.setUnityInfo(
                next(
                    x.value
                    for x in EGameID
                    if x.value is not None and x.value.appid == ugi
                )
            )
        elif isinstance(ugi, EGameID):
            self.setUnityInfo(ugi.value)
        elif isinstance(ugi, UnityGameInfo):
            self.id = ugi.appid
            self.name = ugi.name
            self.unityInfo = ugi

    def deserialize(self, basedir: Path, data: Dict[str, Any]) -> None:
        # print(repr(data))
        self.id = int(data["appid"])
        self.name = data["name"]
        self.path = basedir / "common" / data["installdir"]


class SteamLibrary:
    def __init__(self) -> None:
        self.libraries: Dict[Path, Set[int]] = {}
        self.games: Dict[int, SteamApp] = {}

    def get(self, id: Union[int, EGameID]) -> Optional[SteamApp]:
        return self.games.get(id if isinstance(id, int) else id.value.appid)

    def load(self) -> None:
        # SteamClient is hopelessly broken.
        # We just use it to grab Steam's install location.
        # FIXME: Replace with our own method.
        libvdf = Path(steamclient.STEAM_PATH) / "steamapps" / "libraryfolders.vdf"
        """
        "libraryfolders"
        {
            "contentstatsid"		"-8871853097888873003"
            "0"
            {
                "path"		"C:\\Program Files (x86)\\Steam"
                "label"		""
                "contentid"		"-8871853097888873003"
                "totalsize"		"0"
                "update_clean_bytes_tally"		"3269934185"
                "time_last_update_corruption"		"0"
                "apps"
                {
                    "228980"		"753307118"
        """
        with libvdf.open("r") as f:
            data = vdf.load(f)
            for libid, lib in data["libraryfolders"].items():
                if not libid.isnumeric():
                    continue
                if "path" not in lib:
                    continue
                path = Path(lib["path"])
                appids: Set[int] = set(map(int, lib["apps"].keys()))
                self.libraries[path] = appids

                # hdr = ['appid', 'name', 'path']
                # rows = []
                for appid in appids:
                    acffile: Path
                    # if libid == '0':
                    #     acffile = path / f'appmanifest_{appid}.acf'
                    # else:
                    #     acffile = path / 'steamapps' / f'appmanifest_{appid}.acf'
                    acffile = path / "steamapps" / f"appmanifest_{appid}.acf"
                    if acffile.is_file():
                        try:
                            app = self._loadAppFrom(acffile)
                            if int(libid) not in self.libraries:
                                self.libraries[int(libid)] = set([app.id])
                            else:
                                self.libraries[int(libid)].add(app.id)
                            self.games[app.id] = app
                            # rows += [(app.id, app.name, app.path)]
                        except Exception as e:
                            click.secho(
                                _("W: Failed to load app {appid} from {acffile}").format(
                                    appid=appid,
                                    acffile=acffile
                                ),
                                fg="yellow",
                            )
                            # print(repr(e))
                # print(tabulate.tabulate(rows, headers=hdr, tablefmt='github'))

    def _loadAppFrom(self, path: Path) -> Optional[SteamApp]:
        """
        "AppState"
        {
            "appid"		"881100"
            "universe"		"1"
            "LauncherPath"		"C:\\Program Files (x86)\\Steam\\steam.exe"
            "name"		"Noita"
            "StateFlags"		"4"
            "installdir"		"Noita"
            "LastUpdated"		"1620031758"
            "SizeOnDisk"		"1449387275"
            "StagingSize"		"0"
            "buildid"		"6590931"
            "LastOwner"		"76561197993394396"
            "AutoUpdateBehavior"		"0"
            "AllowOtherDownloadsWhileRunning"		"0"
            "ScheduledAutoUpdate"		"0"
            "InstalledDepots"
            {
                "881101"
                {
                    "manifest"		"654912509375559664"
                    "size"		"1449387275"
                }
            }
            "SharedDepots"
            {
                "228985"		"228980"
            }
            "UserConfig"
            {
                "language"		"english"
            }
            "MountedConfig"
            {
            }
        }
        """
        with path.open("r") as f:
            data = vdf.load(f)
            app = SteamApp()
            app.deserialize(path.parent, data["AppState"])
            return app
